package Lab4;
import java.util.Date;
public class Order {
	public static final int MAX_NUMBER_ORDERED =10;
	public static final int MAX_LIMITED_ORDERS = 5;
	private static int nbOrders;
    private DigitalVideoDisc itemsOrdered[]= new DigitalVideoDisc [MAX_NUMBER_ORDERED];
	private int qtyOrdered;
	private Date dateOrdered;
	public Order() {
	    if (nbOrders == MAX_LIMITED_ORDERS) {
	      System.out.println("Max limited orders in day");
	      return;
	    }
	    this.setDateOrdered();
	    nbOrders++;
	  }

   public int getQtyOrdered() {
		return qtyOrdered;
	 
	}
	public Date getDateOrdered() {
		return dateOrdered;
	}
	public void setDateOrdered(Date dateOrdered) {
		this.dateOrdered = dateOrdered;
	}
	public void setQtyOrdered(int qtyOrdered) {
		this.qtyOrdered = qtyOrdered;
	}
	
	  public void setDateOrdered() {
		    dateOrdered = new Date();;
		  }


	/*public boolean addDigitalVideoDisc(DigitalVideoDisc disc) {
		if(this.qtyOrdered > MAX_NUMBER_ORDERED) return false;
		else {
			itemsOrdered[qtyOrdered] = disc;
			this.qtyOrdered ++;
			return true;
		}
	}*/
	public void addDigitalVideoDisc(DigitalVideoDisc disc) {
	    if (this.qtyOrdered < 10) {
	      itemsOrdered[qtyOrdered] = disc;
	     this. qtyOrdered++;
	      System.out.println("The disc has been added!");
	    } else {
	      System.out.println("The order is almost full!");
	    }
	  }

	public void addDigitalVideoDisc(DigitalVideoDisc[] dvdList) {
	    if (this.qtyOrdered + dvdList.length < 10) {
	      for (int i = 0; i < dvdList.length; i++) {
	        itemsOrdered[qtyOrdered] = dvdList[i];
	      this. qtyOrdered++;
	        System.out.println("The disc has been added!");
	      }
	    } else {
	      System.out.println("The order is almost full!");
	    }
	  }
	public void addDigitalVideoDisc(DigitalVideoDisc dvd1, DigitalVideoDisc dvd2) {
	    if (this.qtyOrdered + 2 < 10) {
	      itemsOrdered[qtyOrdered] = dvd1;
	     this. qtyOrdered++;
	      itemsOrdered[qtyOrdered] = dvd2;
	      this.qtyOrdered++;
	      System.out.println("The disc has been added!");
	    } else {
	      System.out.println("The order is almost full!");
	    }
	  }

   public boolean removeDigitalVideoDisc( DigitalVideoDisc disc) { 
		int a, i;
		boolean result = false;
		for( a = i = 0; i < this.qtyOrdered ; i++) {
			if(this.itemsOrdered[i].getTitle() != disc.getTitle()) {
				this.itemsOrdered[a] = this.itemsOrdered[i];
				a++;
			}
		}
		if(this.qtyOrdered != a) {
			this.setQtyOrdered(a);
			result = true;
		}
		return result;
		
	 }

	public float totalCost() {
		float totalCost = 0;
		int i;
		for(i = 0; i < this.qtyOrdered; i++) {
			totalCost += itemsOrdered[i].getCost();
		}
		return totalCost;
	}


public void printOrder() {
    System.out.println("****************-------ORDER------******************");
    System.out.println(" Show Date: " + dateOrdered);
    System.out.println("Show Ordered items:");
    for (int a = 0; a < qtyOrdered; a++) {
      System.out.println(a + 1 + ". DVD - " + itemsOrdered[a].getTitle() + " - " + itemsOrdered[a].getCategory() + " - " + itemsOrdered[a].getDirector() + " - " + itemsOrdered[a].getLength() + ": " + itemsOrdered[a].getCost() + "$");
    }
    System.out.println("Total cost: " + this.totalCost());
    System.out.println("********----END-----------************");
  }
}

