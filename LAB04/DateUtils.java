package Lab4;

public class DateUtils {
	public static int compareDate(MyDate date1, MyDate date2) {
	    int yearHT = date1.getYear() - date2.getYear();
	    if (yearHT> 0) {
	      return 1;
	    } else if (yearHT == 0) {
	      int monthHT = date1.getMon() - date2.getMon();
	      if (monthHT > 0) {
	        return 1;
	      } else if (monthHT == 0) {
	        int dayHT = date1.getDay() - date2.getDay();
	        if (dayHT > 0) {
	          return 1;
	        } else if (dayHT == 0) {
	          return 0;
	        }
	      }
	    }
	    return -1;
	  }

}
