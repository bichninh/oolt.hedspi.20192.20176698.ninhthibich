package aims.media;
public class DigitalVideoDisc extends Disc implements Playable, Comparable {
	private String director;
	private int length;
	public DigitalVideoDisc() {
	  }
	public DigitalVideoDisc(int id) {
	    super(id);
	  }
	
   
   public String getDirector() {
		return director;
	}
	public void setDirector(String director) {
		this.director = director;
	}
	public int getLength() {
		return length;
	}
	 
	 public void setLength(int length) {
		    this.length = length;
		  }   
	
		
     public DigitalVideoDisc(String title, String category) {
		    super(title, category);
		  }
      
	 public DigitalVideoDisc(String title, String category, String director, int length) {
		    super(title, category);
		    this.director = director;
		    this.length = length;
		  }
// override
	  public void play() {
		    System.out.println("Playing track: " + this.getTitle());
		    System.out.println("Track length: " + this.getLength());
		  }
//override
	  public int compareTo(Object obj) {
		    DigitalVideoDisc dvd = (DigitalVideoDisc) obj;
             if (this.getCost() < dvd.getCost()) return -1;
		    else if (this.getCost() > dvd.getCost()) return 1;
		    return 0;
		  }

	
}