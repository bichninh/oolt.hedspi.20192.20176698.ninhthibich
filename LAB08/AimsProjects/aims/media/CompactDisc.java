package aims.media;
import java.util.ArrayList;

public class CompactDisc extends Disc implements Playable, Comparable {
	  private String artist;
	  private int length;
	  private ArrayList<Track> tracks = new ArrayList<Track>();
	  public CompactDisc() {
			// TODO Auto-generated constructor stub
		}
	public String getArtist() {
		return artist;
	}

	public void setArtist(String artist) {
		this.artist = artist;
	}

	
    public void addTrack(Track track) {
		if (tracks.contains(track)) {
		System.out.println("Track is already in the list of tracks!");
		      return;
		    }
	     tracks.add(track);
		System.out.println("addTrack success!");
		  }
    public void removeTrack(Track track) {
        if (tracks.remove(track)) {
          System.out.println("removeTrack success!");
          return;
        }
        System.out.println("Track is not in the list of tracks!");
      }
    public int getLength() {
        int length = 0;
        for (int a = 0; a < tracks.size(); a++) {
          length += tracks.get(a).getLength();
        }
        return length;
      }
//override
    public void play() {
        System.out.println("Playing " + this.getArtist() + "'s disc");
        for (int a = 0; a < tracks.size(); a++) {
          tracks.get(a).play();
        }
      }
 //override
    
    public int compareTo(Object obj) {
    	 CompactDisc CD = (CompactDisc) obj;

    	 if (tracks.size() > CD.tracks.size()) return 1;
    	 else if (tracks.size() < CD.tracks.size()) return -1;
    	 else {
    	    if (this.getLength() > CD.getLength()) return 1;
    	    else if (this.getLength() < CD.getLength()) return -1;
    	    return 0;
    	    }
    	}
    }


