package aims.Aims;
import aims.media.Book;
import aims.media.DigitalVideoDisc;
import aims.media.Media;
import aims.order.Order.*;
import java.util.ArrayList;
import java.util.Scanner;
import aims.media.CompactDisc;
import aims.media.Track;

public class Aims{
	private static ArrayList<Order> ordersList = new ArrayList<Order>();
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		MemoryDaemon md = new MemoryDaemon();
	    Thread thread = new Thread(md);
	    thread.setDaemon(true);
		Scanner s = new Scanner(System.in);
	    int luachon;
	  do {
	    	showMenu();
	    	luachon = s.nextInt();
	    	switch (luachon) {
	    	case 1:
	    		System.out.println("1. Create new order.");
	            Order order = new Order();
	            ordersList.add(order);
	            System.out.println("New order has been created!");
	            break;
	    	case 2:
	    	{
	    		Scanner a = new Scanner(System.in);
	    		int lc;
				do {
		 		Show();
		 		lc= s.nextInt();
		 		switch (lc) {
		 		case 1:
					Book book =addBook();
					ordersList.get(ordersList.size() - 1).addMedia(book);
					
		 		break;
		 		case 2:
					CompactDisc cd= addcompactdisc();
					 ordersList.get(ordersList.size() - 1).addMedia(cd);
		    		System.out.print("Enter 1 to run:");
		    		int lc1 = s.nextInt();
		    		if(lc1==1){
		    				cd.play();
		    			}
		 		break;
				case 3:
					DigitalVideoDisc dvd= addDVD();
					ordersList.get(ordersList.size() - 1).addMedia(dvd);
					System.out.print("Enter 1 to run :");
		    		int lc2 = s.nextInt();
		    		if(lc2==1){
		    				dvd.play();
		    			}
				break;
				case 0:
	    	          System.out.println("Exit!");
	    	          break;

	    	      default:
	    	          System.out.println("Invalid!");
	    	          break;
		 		}
		 		}while( lc!= 0);
				break;
	    	}
			case 3:
	        	System.out.println("3. Delete item by id.");
	            System.out.print("Enter id: ");
	            int deleteItem = s.nextInt();
	            ordersList.get(ordersList.size() - 1).removeMedia(deleteItem);
	            break;
	        case 4:
	        	System.out.println("4. Display the items list of order.");
	            ordersList.get(ordersList.size() - 1).printOrder();
	            break;

	    	case 0:
	            System.out.println("Exit!");
	            break;

	        default:
	            System.out.println("Invalid!");
	            break;
	    	}
	    } while( luachon!=0);
	    
	}
	  
	   public static void showMenu() {
	        System.out.println("Order Management Application: ");
	        System.out.println("--------------------------------");
	        System.out.println("1. Create new order.");
	        System.out.println("2. Add item to the order.");
	        System.out.println("3. Delete item by id.");
	        System.out.println("4. Display the items list of order.");
	        System.out.println("0. Exit.");
	        System.out.println("--------------------------------");
	        System.out.println("Please choose a number: 0-1-2-3-4 ");
	   }
	   public static void Show() {
		  System.out.println("1. Book");
	      System.out.println("2. Compact disc");
	      System.out.println("3. DVD");
	      System.out.println("0. Exit!");
	      System.out.print("Please choose a number: 0-1-2-3.");
	   }
	   public static Book addBook(){
			Book book= new Book();
			Scanner sc = new Scanner(System.in);
			System.out.print("Title: ");
			book.setTitle(sc.nextLine());
			System.out.print("Category: ");
			book.setCategory(sc.nextLine());
			System.out.print("Cost: ");
			book.setCost(Float.parseFloat(sc.nextLine()));
			int a=1;
			int i=1;
			do{
				if(a==1){
					System.out.print("Author "+i+": ");
					String author= sc.nextLine();
					book.addAuthor(author);
					i++;
				}		
				System.out.print("Enter a=1: ");
				a= Integer.parseInt(sc.nextLine());
			}while(a==1);
			return book;
		}
	   public static CompactDisc addcompactdisc(){
		   CompactDisc cd = new CompactDisc();
			Scanner sc = new Scanner(System.in);
			System.out.print("Title: ");
			cd.setTitle(sc.nextLine());
			System.out.print("Category: ");
			cd.setCategory(sc.nextLine());
			System.out.print("Director: ");
			cd.setDirector(sc.nextLine());
			System.out.print("Artist: ");
			cd.setArtist(sc.nextLine());
			System.out.print("Cost: ");
			cd.setCost(Float.parseFloat(sc.nextLine()));
			int a=1;
			int i=1;
			while(a==1){
				Track track = new Track();
				System.out.print("add track with ");
				System.out.print("Title is"+ i +": ");
				track.setTitle(sc.nextLine());
				System.out.print("Length is "+ i +": ");
				track.setLength(Integer.parseInt(sc.nextLine()));
				cd.addTrack(track);
				i++;
				System.out.print("Enter a=1:");
				a= Integer.parseInt(sc.nextLine());		
			}
			return cd;
		}
	   public static DigitalVideoDisc addDVD(){
			DigitalVideoDisc dvd = new DigitalVideoDisc();
		    Scanner sc = new Scanner(System.in);
			System.out.print("Title: ");
			dvd.setTitle(sc.nextLine());
			System.out.print("Length: ");
			dvd.setLength(Integer.parseInt(sc.nextLine()));
			System.out.print("Cost: ");
			dvd.setCost(Float.parseFloat(sc.nextLine()));
			System.out.print("Category: ");
			dvd.setCategory(sc.nextLine());
			System.out.print("Director: ");
			dvd.setDirector(sc.nextLine());
			return dvd;
		}
	  
	     
}
