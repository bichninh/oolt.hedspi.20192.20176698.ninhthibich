package aims.media;
import java.util.ArrayList;
import aims.PlayerException;


public class CompactDisc extends Disc implements Playable, Comparable {
	  private String artist;
	  private int length;
	  private ArrayList<Track> tracks = new ArrayList<Track>();
	  public CompactDisc() {
			// TODO Auto-generated constructor stub
		}
	public String getArtist() {
		return artist;
	}

	public void setArtist(String artist) {
		this.artist = artist;
	}

	
    public void addTrack(Track track) {
		if (tracks.contains(track)) {
		System.out.println("Track is already in the list of tracks!");
		      return;
		    }
	     tracks.add(track);
		System.out.println("addTrack success!");
		  }
    public void removeTrack(Track track) {
        if (tracks.remove(track)) {
          System.out.println("removeTrack success!");
          return;
        }
        System.out.println("Track is not in the list of tracks!");
      }
    public int getLength() {
        int length = 0;
        for (int a = 0; a < tracks.size(); a++) {
          length += tracks.get(a).getLength();
        }
        return length;
      }

        
      
  //Override
    public void play() throws PlayerException {
      if (this.getLength() <= 0) {
        System.err.println("ERROR: CD length is 0");
        throw new PlayerException();
      }

      System.out.println("Playing " + this.getArtist() + "'s disc");
      System.out.println("Playing CD: " + this.getTitle());
      System.out.println("CD length: " + this.getLength());

      for (int a = 0; a < tracks.size(); a++) {
        try {
          tracks.get(a).play();
        } catch (PlayerException ob) {
          ob.printStackTrace();
        }
      }
    }
 //override
    
    public int compareTo(Object obj) {
    	 CompactDisc CD = (CompactDisc) obj;

    	 if (tracks.size() > CD.tracks.size()) return 1;
    	 else if (tracks.size() < CD.tracks.size()) return -1;
    	 else {
    	    if (this.getLength() > CD.getLength()) return 1;
    	    else if (this.getLength() < CD.getLength()) return -1;
    	    return 0;
    	    }
    	}
    }


