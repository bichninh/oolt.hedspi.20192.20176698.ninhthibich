package aims.Aims;
import aims.media.DigitalVideoDisc;
import aims.media.Media;
import aims.order.Order.*;
import java.util.ArrayList;
import java.util.Scanner;

public class Aims{
    
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner s = new Scanner(System.in);
	    int luachon;
	    ArrayList<Order> ordersList = new ArrayList<Order>();
	
	    DigitalVideoDisc dvd1 = new DigitalVideoDisc("The Lion King","Animation","Roger Allers",87);
	    dvd1.setId(1);
	    dvd1.setCost(19.95f);
	    
	    DigitalVideoDisc dvd2 = new DigitalVideoDisc("Star Wars","Science Fiction","George Lucas",124);
	    dvd2.setId(2);
	    dvd2.setCost(24.95f);
	    
	    DigitalVideoDisc dvd3 = new DigitalVideoDisc("Aladi","fehdkw","Mier Musker",90);
	    dvd3.setId(3);
	    dvd3.setCost(18.99f);
	    
       DigitalVideoDisc dvd4 = new DigitalVideoDisc("Stay at home","abcs","John Hider",100);
	    dvd4.setId(4);
	    dvd4.setCost(20.00f);
	    
	    
	    
	    do {
	    	showMenu();
	    	luachon = s.nextInt();
	    	switch (luachon) {
	    	case 1:
	    		System.out.println("1. Create new order.");
	            Order order = new Order();
	            ordersList.add(order);
	            System.out.println("New order has been created!");
	            break;
	    	case 2:
	            System.out.println("2. Add item to the order.");
	            ordersList.get(ordersList.size() - 1).addMedia(dvd1);
	            ordersList.get(ordersList.size() - 1).addMedia(dvd2);
	            ordersList.get(ordersList.size() - 1).addMedia(dvd3);
	            ordersList.get(ordersList.size() - 1).addMedia(dvd4);
	            break;
	        case 3:
	        	System.out.println("3. Delete item by id.");
	            System.out.print("Enter id: ");
	            int deleteItem = s.nextInt();
	            ordersList.get(ordersList.size() - 1).removeMedia(deleteItem);
	            break;
	        case 4:
	        	System.out.println("4. Display the items list of order.");
	            ordersList.get(ordersList.size() - 1).printOrder();
	            break;

	    	case 0:
	            System.out.println("Exit!");
	            break;

	        default:
	            System.out.println("Invalid!");
	            break;
	    	}
	    } while( luachon!=0);
	}
	    
	    
	    public static void showMenu() {
	        System.out.println("Order Management Application: ");
	        System.out.println("--------------------------------");
	        System.out.println("1. Create new order.");
	        System.out.println("2. Add item to the order.");
	        System.out.println("3. Delete item by id.");
	        System.out.println("4. Display the items list of order.");
	        System.out.println("0. Exit.");
	        System.out.println("--------------------------------");
	        System.out.println("Please choose a number: 0-1-2-3-4 ");
	      
	    

	}

}
