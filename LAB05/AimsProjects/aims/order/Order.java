package aims.order.Order;
import aims.disc.DigitalVideoDisc.DigitalVideoDisc;
import java.util.Date;

public class Order {
	public static final int MAX_NUMBER_ORDERED =10;
	public static final int MAX_LIMITED_ORDERS = 5;
	private static int nbOrders =0;
    private DigitalVideoDisc itemsOrdered[]= new DigitalVideoDisc [MAX_NUMBER_ORDERED];
	private int qtyOrdered;
	private Date dateOrdered;
	public Order() {
	    if (nbOrders == MAX_LIMITED_ORDERS) {
	      System.out.println("Max limited orders in day");
	      return;
	    }
	    this.setDateOrdered();
	    nbOrders++;
	  }

   public int getQtyOrdered() {
		return qtyOrdered;
	 
	}
	public Date getDateOrdered() {
		return dateOrdered;
	}
	public void setDateOrdered(Date dateOrdered) {
		this.dateOrdered = dateOrdered;
	}
	public void setQtyOrdered(int qtyOrdered) {
		this.qtyOrdered = qtyOrdered;
	}
	
	  public void setDateOrdered() {
		    dateOrdered = new Date();;
		  }


	/*public boolean addDigitalVideoDisc(DigitalVideoDisc disc) {
		if(this.qtyOrdered > MAX_NUMBER_ORDERED) return false;
		else {
			itemsOrdered[qtyOrdered] = disc;
			this.qtyOrdered ++;
			return true;
		}
	}*/
	public void addDigitalVideoDisc(DigitalVideoDisc dvd1 ) {
	    if (qtyOrdered < 10) {
	      itemsOrdered[qtyOrdered] = dvd1;
	      qtyOrdered++;
	      System.out.println("The disc has been added!");
	    } else {
	      System.out.println("The order is almost full!");
	    }
	  }

	public void addDigitalVideoDisc(DigitalVideoDisc[] dvdList) {
	    if (qtyOrdered + dvdList.length < 10) {
	      for (int i = 0; i < dvdList.length; i++) {
	        itemsOrdered[qtyOrdered] = dvdList[i];
	      qtyOrdered++;
	        System.out.println("The disc has been added!");
	      }
	    } else {
	      System.out.println("The order is almost full!");
	    }
	  }
	public void addDigitalVideoDisc(DigitalVideoDisc dvd1, DigitalVideoDisc dvd2) {
	    if (qtyOrdered + 2 < 10) {
	      itemsOrdered[qtyOrdered] = dvd1;
	      qtyOrdered++;
	      itemsOrdered[qtyOrdered] = dvd2;
	      qtyOrdered++;
	      System.out.println("The disc has been added!");
	    } else {
	      System.out.println("The order is almost full!");
	    }
	  }

   public boolean removeDigitalVideoDisc(DigitalVideoDisc disc) { 
		int a, i;
		boolean result = false;
		for( a = i = 0; i < this.qtyOrdered ; i++) {
			if(this.itemsOrdered[i].getTitle() != disc.getTitle()) {
				this.itemsOrdered[a] = this.itemsOrdered[i];
				a++;
			}
		}
		if(this.qtyOrdered != a) {
			this.setQtyOrdered(a);
			result = true;
		}
		return result;
		
	 }

	public float totalCost() {
		float totalCost = 0;
		int i;
		for(i = 0; i < this.qtyOrdered; i++) {
			totalCost += itemsOrdered[i].getCost();
		}
		return totalCost;
	}
	public boolean Search(String title) {
		for(int a = 0; a < this.qtyOrdered; a++) {
			String tmp= new String();
			tmp= this.itemsOrdered[a].getTitle();
			if ( title.equalsIgnoreCase(tmp)) {
				System.out.println(tmp);
				return true;
				}else {
					if(tmp.contains(title)) {
						System.out.println(tmp);
						return true;
					}
				}
	}
		return false;
	}
	public DigitalVideoDisc getALuckyItem() {
	    int Dvdbk = (int)(qtyOrdered * Math.random());
	    itemsOrdered[Dvdbk].setCost(0);
	    return itemsOrdered[Dvdbk];
	  }
   
public void printOrder() {
    System.out.println("****************-------ORDER------******************");
    System.out.println(" Show Date: " + dateOrdered);
    System.out.println("Show Ordered items:");
    for (int a = 0; a < qtyOrdered; a++) {
      System.out.println(a + 1 + ". DVD - " + itemsOrdered[a].getTitle() + " - " + itemsOrdered[a].getCategory() + " - " + itemsOrdered[a].getDirector() + " - " + itemsOrdered[a].getLength() + ": " + itemsOrdered[a].getCost() + "$");
    }
    System.out.println("Total cost: " + this.totalCost());
    System.out.println("********----END-----------************");
  }


}


