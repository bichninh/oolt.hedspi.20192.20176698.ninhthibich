package garbage;
import java.io.*;
import java.nio.file.*;
import java.util.Scanner; 

public class GarbageCreator {
    
   public static String readFileAsString(String fileName)throws Exception 
    { 
      String s= ""; 
      Scanner sc = new Scanner(Paths.get(fileName),"UTF-8");
      while(sc.hasNextLine()){
        String tmp = sc.nextLine();
        s += tmp;
      }
      sc.close();
      return s; 
    } 
    
    public static void main(String[] args) throws Exception 
    { 
    long start = System.currentTimeMillis();
      String s = readFileAsString("C:\\Users\\HP\\Desktop\\FileTest.txt"); 
      System.out.println(s); 
      System.out.println(System.currentTimeMillis()-start);
    } 

}
