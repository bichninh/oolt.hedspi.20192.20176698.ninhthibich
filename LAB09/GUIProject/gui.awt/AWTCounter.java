package gui.awt;
import java.awt.*;
import java.awt.event.*;

public class AWTCounter extends Frame implements ActionListener {
	  private Label lblCount;  // Declare a Label component
	  private TextField tfCount; // Declare a TextField component 
      private Button btnCount;  // Declare a Button component
      private int count = 0;    // Counter's value
   // Constructor to setup GUI components and event handlers
	  public AWTCounter() {
	    setLayout(new FlowLayout());

	    lblCount = new Label("Counter");
	    add(lblCount);

	    tfCount = new TextField(count + "", 10);
	    tfCount.setEditable(false);
	    add(tfCount);

	    btnCount = new Button("Count");
	    add(btnCount);
	    btnCount.addActionListener(this);

	    setTitle("AWT Counter");
	    setSize(250, 100);

	    setVisible(true);
	  }
 
	  public static void main(String[] args) {
		    AWTCounter app = new AWTCounter();
		  }

		  @Override
		  public void actionPerformed(ActionEvent actionEvent) {
		    count++;
		    tfCount.setText(count + "");
		  }
		}

	